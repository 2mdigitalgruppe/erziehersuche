<x-guest-layout>


    <div class = "container-fluid" >
        <div class="row">
            <div class="col-xl-7 col-lg-9 col-md-10 ml-auto mr-auto">
                <img class="img-fluid" src="/images/header_impressum.jpg">
            </div>
        </div>
    </div>

    <div class = "container-fluid">
        <div class = "row py-5 policy">
            <div class = "col-xl-7 col-lg- col-md-10 ml-auto mr-auto">
                
                <span class="text-uppercase py-2 px-3 text-white bg-primary headline">Datenschutz</span>

                <p class="pt-4">Die Betreiber dieser Seiten nehmen den Schutz Ihrer persönlichen Daten sehr ernst. Wir behandeln Ihre personenbezogenen Daten vertraulich und entsprechend der gesetzlichen Datenschutzvorschriften sowie dieser Datenschutzerklärung.</p>

                <h2>Begriffsbestimmungen</h2>
                <h3>nach Artikel 4 Nr.1 bis 11 Datenschutzgrundverordnung (DSGVO)</h3>

                <p>Der Gesetzgeber fordert, dass personenbezogene Daten auf rechtmäßige Weise, nach Treu und Glauben und in einer für die betroffene Person nachvollziehbaren Weise verarbeitet werden („Rechtmäßigkeit, Verarbeitung nach Treu und Glauben, Transparenz“). Um dies zu gewährleisten, informieren wir Sie über die einzelnen gesetzlichen Begriffsbestimmungen, die auch in dieser Datenschutzerklärung verwendet werden:</p>

                <h4>1. Personenbezogene Daten</h4>

                <p>„Personenbezogene Daten“ sind alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person (im Folgenden „betroffene Person“) beziehen; als identifizierbar wird eine natürliche Person angesehen, die direkt oder indirekt, insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung oder zu einem oder mehreren besonderen Merkmalen identifiziert werden kann, die Ausdruck der physischen, physiologischen, genetischen, psychischen, wirtschaftlichen, kulturellen oder sozialen Identität dieser natürlichen Person sind.</p>

                <h4>2. Verarbeitung</h4>

                <p>„Verarbeitung“ ist jeder, mit oder ohne Hilfe automatisierter Verfahren, ausgeführter Vorgang oder jede solche Vorgangsreihe im Zusammenhang mit personenbezoge-nen Daten wie das Erheben, das Erfassen, die Organisation, das Ordnen, die Spei-cherung, die Anpassung oder Veränderung, das Auslesen, das Abfragen, die Ver-wendung, die Offenlegung durch Übermittlung, Verbreitung oder eine andere Form der Bereitstellung, den Abgleich oder die Verknüpfung, die Einschränkung, das Lö-schen oder die Vernichtung.</p>

                <h4>3. Einschränkung der Verarbeitung</h4>

                <p>„Einschränkung der Verarbeitung“ ist die Markierung gespeicherter personenbezogener Daten mit dem Ziel, ihre künftige Verarbeitung einzuschränken.</p>

                <h4>4. Profiling</h4>

                <p>„Profiling“ ist jede Art der automatisierten Verarbeitung personenbezogener Daten, die darin besteht, dass diese personenbezogenen Daten verwendet werden, um bestimmte persönliche Aspekte, die sich auf eine natürliche Person beziehen, zu bewerten, insbesondere um Aspekte bezüglich Arbeitsleistung, wirtschaftliche Lage, Gesundheit, persönliche Vorlieben, Interessen, Zuverlässigkeit, Verhalten, Aufenthaltsort oder Ortswechsel dieser natürlichen Person zu analysieren oder vorherzusagen.</p>

                <h4>5. Pseudonymisierung</h4>

                <p>„Pseudonymisierung“ ist die Verarbeitung personenbezogener Daten in einer Weise, dass die personenbezogenen Daten ohne Hinzuziehung zusätzlicher Informationen nicht mehr einer spezifischen betroffenen Person zugeordnet werden können, sofern diese zusätzlichen Informationen gesondert aufbewahrt werden und technischen und organisatorischen Maßnahmen unterliegen, die gewährleisten, dass die personenbezogenen Daten nicht einer identifizierten oder identifizierbaren natürlichen Person zugewiesen werden können.</p>

                <h4>6. Dateisystem</h4>

                <p>„Dateisystem“ ist jede strukturierte Sammlung personenbezogener Daten, die nach bestimmten Kriterien zugänglich sind, unabhängig davon, ob diese Sammlung zentral, dezentral oder nach funktionalen oder geografischen Gesichtspunkten geordnet geführt wird.</p>

                <h4>7. Verantwortlicher</h4>

                <p>„Verantwortlicher“ ist eine natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, die allein oder gemeinsam mit anderen über die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten entscheidet; sind die Zwecke und Mittel dieser Verarbeitung durch das Unionsrecht oder das Recht der Mitgliedstaaten vorgegeben, so können der Verantwortliche beziehungsweise die bestimmten Kriterien seiner Benennung nach dem Unionsrecht oder dem Recht der Mitgliedstaaten vorgesehen werden.</p>

                <h4>8. Auftragsverarbeiter</h4>

                <p>„Auftragsverarbeiter“ ist eine natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, die personenbezogene Daten im Auftrag des Verantwortlichen verarbeitet.</p>

                <h4>9. Empfänger</h4>

                <p>„Empfänger“ ist eine natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, denen personenbezogene Daten offengelegt werden, unabhängig davon, ob es sich bei ihr um einen Dritten handelt oder nicht. Behörden, die im Rahmen eines bestimmten Untersuchungsauftrags nach dem Unionsrecht oder dem Recht der Mitgliedstaaten möglicherweise personenbezogene Daten erhalten, gelten jedoch nicht als Empfänger; die Verarbeitung dieser Daten durch die genannten Behörden erfolgt im Einklang mit den geltenden Datenschutzvorschriften gemäß den Zwecken der Verarbeitung.</p>

                <h4>10. Dritter</h4>

                <p>„Dritter“ ist eine natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, außer der betroffenen Person, dem Verantwortlichen, dem Auftragsverarbeiter und den Personen, die unter der unmittelbaren Verantwortung des Verantwortlichen oder des Auftragsverarbeiters befugt sind, die personenbezogenen Daten zu verarbeiten.</p>

                <h4>11. Einwilligung</h4>

                <p>Eine „Einwilligung“ der betroffenen Person ist jede freiwillig für den bestimmten Fall, in informierter Weise und unmissverständlich abgegebene Willensbekundung in Form einer Erklärung oder einer sonstigen eindeutigen bestätigenden Handlung, mit der die betroffene Person zu verstehen gibt, dass sie mit der Verarbeitung der sie betreffenden personenbezogenen Daten einverstanden ist.</p>

                <h3>Rechtmäßigkeit der Verarbeitung</h3>

                <p>Die Verarbeitung personenbezogener Daten ist nach Art. 6 Abs. 1 DSGVO nur rechtmäßig, wenn mindestens eine der nachstehenden Bedingungen erfüllt ist:</p>

                <p>a) Die betroffene Person hat ihre Einwilligung zu der Verarbeitung der sie betreffenden personenbezogenen Daten für einen oder mehrere bestimmte Zwecke gegeben;</p>

                <p>b) die Verarbeitung ist für die Erfüllung eines Vertrags, dessen Vertragspartei die betroffene Person ist, oder zur Durchführung vorvertraglicher Maßnahmen erforderlich, die auf Anfrage der betroffenen Person erfolgen;</p>

                <p>c) die Verarbeitung ist zur Erfüllung einer rechtlichen Verpflichtung erforderlich, der der Verantwortliche unterliegt;</p>

                <p>d) die Verarbeitung ist erforderlich, um lebenswichtige Interessen der betroffenen Person oder einer anderen natürlichen Person zu schützen;</p>

                <p>e) die Verarbeitung ist für die Wahrnehmung einer Aufgabe erforderlich, die im öffentlichen Interesse liegt oder in Ausübung öffentlicher Gewalt erfolgt, die dem Verantwortlichen übertragen wurde;</p>

                <p>f) die Verarbeitung ist zur Wahrung der berechtigten Interessen des Verantwort-lichen oder eines Dritten erforderlich, sofern nicht die Interessen oder Grund-rechte und Grundfreiheiten der betroffenen Person, die den Schutz personenbezogener Daten erfordern, überwiegen, insbesondere dann, wenn es sich bei der betroffenen Person um ein Kind handelt.</p>

                <h3>Rechte der betroffenen Person</h3>

                <h4>(1) Widerruf der Einwilligung</h4>

                <p>Sofern die Verarbeitung der personenbezogenen Daten auf einer erteilten Einwilli-gung beruht, haben Sie jederzeit das Recht, die Einwilligung zu widerrufen. Durch den Widerruf der Einwilligung wird die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung nicht berührt. Für die Ausübung des Widerrufsrechts können Sie sich jederzeit an den o.g. Verantwortlichen wenden.</p>

                <h4>(2) Recht auf Bestätigung</h4>

                Sie haben das Recht, von dem Verantwortlichen eine Bestätigung darüber zu verlangen, ob wir Sie betreffende personenbezogene Daten verarbeiten. Die Bestätigung können Sie jederzeit unter den oben genannten Kontaktdaten verlangen.

                <h4>(3) Auskunftsrecht</h4>

                <p>Sofern personenbezogene Daten verarbeitet werden, können Sie jederzeit Auskunft über diese personenbezogenen Daten und über folgenden Informationen verlangen:</p>

                <p>a) die Verarbeitungszwecke;</p>

                <p>b) die Kategorien personenbezogener Daten, die verarbeitet werden;</p>

                <p>c) die Empfänger oder Kategorien von Empfängern, gegenüber denen die per-sonenbezogenen Daten offengelegt worden sind oder noch offengelegt werden, insbesondere bei Empfängern in Drittländern oder bei internationalen Organisationen;</p>

                <p>d) falls möglich, die geplante Dauer, für die die personenbezogenen Daten ge-speichert werden, oder, falls dies nicht möglich ist, die Kriterien für die Festle-gung dieser Dauer;</p>

                <p>e) das Bestehen eines Rechts auf Berichtigung oder Löschung der Sie betref-fenden personenbezogenen Daten oder auf Einschränkung der Verarbeitung durch den Verantwortlichen oder eines Widerspruchsrechts gegen diese Verarbeitung;</p>

                <p>f) das Bestehen eines Beschwerderechts bei einer Aufsichtsbehörde;</p>

                <p>g) wenn die personenbezogenen Daten nicht bei Ihnen erhoben werden, alle verfügbaren Informationen über die Herkunft der Daten;</p>

                <p>h) das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling gemäß Artikel 22 Absätze 1 und 4 DSGVO und – zumindest in diesen Fällen – aussagekräftige Informationen über die involvierte Logik sowie die Tragweite und die angestrebten Auswirkungen einer derartigen Verarbeitung für Sie.</p>

                <p>Werden personenbezogene Daten an ein Drittland oder an eine internationale Organisation übermittelt, so haben Sie das Recht, über die geeigneten Garantien gemäß Artikel 46 DSGVO im Zusammenhang mit der Übermittlung unterrichtet zu werden. Wir stellen eine Kopie der personenbezogenen Daten, die Gegenstand der Verarbeitung sind, zur Verfügung. Für alle weiteren Kopien, die Sie beantragen, können wir ein angemessenes Entgelt auf der Grundlage der Verwaltungskosten verlangen. Stellen Sie den Antrag elektronisch, so sind die Informationen in einem gängigen elektronischen Format zur Verfügung zu stellen, sofern Sie nichts anderes angeben.</p>

                <h4>(4) Recht auf Berichtigung</h4>

                <p>Sie haben das Recht, von uns unverzüglich die Berichtigung Sie betreffender unrichtiger personenbezogener Daten zu verlangen. Unter Berücksichtigung der Zwecke der Verarbeitung haben Sie das Recht, die Vervollständigung unvollständiger personenbezogener Daten – auch mittels einer ergänzenden Erklärung – zu verlangen.</p>

                <h4>(5) Recht auf Löschung</h4>

                <p>(„Recht auf Vergessenwerden“) Sie haben das Recht, von dem Verantwortlichen zu verlangen, dass Sie betreffende personenbezogene Daten unverzüglich gelöscht werden, und wir sind verpflichtet, personenbezogene Daten unverzüglich zu löschen, sofern einer der folgenden Gründe zutrifft:</p>

                <p>a) Die personenbezogenen Daten sind für die Zwecke, für die sie erhoben oder auf sonstige Weise verarbeitet wurden, nicht mehr notwendig.</p>

                <p>b) Sie widerrufen Ihre Einwilligung, auf die sich die Verarbeitung gemäß Artikel 6 Absatz 1 Buchstabe a oder Artikel 9 Absatz 2 Buchstabe a DSGVO stützte, und es fehlt an einer anderweitigen Rechtsgrundlage für die Verarbeitung.</p>

                <p>c) Sie legen gemäß Artikel 21 Absatz 1 DSGVO Widerspruch gegen die Verar-beitung ein und es liegen keine vorrangigen berechtigten Gründe für die Ver-arbeitung vor, oder Sie legen gemäß Artikel 21 Absatz 2 DSGVO Widerspruch gegen die Verarbeitung ein.</p>

                <p>d) Die personenbezogenen Daten wurden unrechtmäßig verarbeitet.</p>

                <p>e) Die Löschung der personenbezogenen Daten ist zur Erfüllung einer rechtli-chen Verpflichtung nach dem Unionsrecht oder dem Recht der Mitgliedstaaten erforderlich, dem der Verantwortliche unterliegt.</p>

                <p>f) Die personenbezogenen Daten wurden in Bezug auf angebotene Dienste der Informationsgesellschaft gemäß Artikel 8 Absatz 1 DSGVO erhoben.</p>

                <p>Hat der Verantwortliche die personenbezogenen Daten öffentlich gemacht und ist er zu deren Löschung verpflichtet, so trifft er unter Berücksichtigung der verfügbaren Technologie und der Implementierungskosten angemessene Maßnahmen, auch technischer Art, um für die Datenverarbeitung Verantwortliche, die die personenbezogenen Daten verarbeiten, darüber zu informieren, dass eine betroffene Person von ihnen die Löschung aller Links zu diesen personenbezogenen Daten oder von Kopien oder Replikationen dieser personenbezogenen Daten verlangt hat.</p>

                <p>Das Recht auf Löschung („Recht auf Vergessenwerden“) besteht nicht, soweit die Verarbeitung erforderlich ist:</p>

                <p>• zur Ausübung des Rechts auf freie Meinungsäußerung und Information;</p>

                <p>• zur Erfüllung einer rechtlichen Verpflichtung, die die Verarbeitung nach dem Recht der Union oder der Mitgliedstaaten, dem der Verantwortliche unterliegt, erfordert, oder zur Wahrnehmung einer Aufgabe, die im öffentlichen Interesse liegt oder in Ausübung öffentlicher Gewalt erfolgt, die dem Verantwortlichen übertragen wurde;</p>

                <p>• aus Gründen des öffentlichen Interesses im Bereich der öffentlichen Gesundheit gemäß Artikel 9 Absatz 2 Buchstaben h und i sowie Artikel 9 Absatz 3 DSGVO;</p>

                <p>• für im öffentlichen Interesse liegende Archivzwecke, wissenschaftliche oder historische Forschungszwecke oder für statistische Zwecke gemäß Artikel 89 Absatz 1 DSGVO, soweit das in Absatz 1 genannte Recht voraussichtlich die Verwirklichung der Ziele dieser Verarbeitung unmöglich macht oder ernsthaft beeinträchtigt, oder</p>

                <p>• zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen. (6) Recht auf Einschränkung der Verarbeitung Sie haben das Recht, von uns die Einschränkung der Verarbeitung ihrer personenbezogenen Daten zu verlangen, wenn eine der folgenden Voraussetzungen gegeben ist:</p>

                <p>a) wenn die Richtigkeit der personenbezogenen Daten von Ihnen bestritten wird, und zwar für eine Dauer, die es dem Verantwortlichen ermöglicht, die Richtigkeit der personenbezogenen Daten zu überprüfen,</p>

                <p>b) wenn die Verarbeitung unrechtmäßig ist und Sie die Löschung der personenbezogenen Daten ablehnen und stattdessen die Einschränkung der Nutzung der personenbezogenen Daten verlangen;</p>

                <p>c) wenn der Verantwortliche die personenbezogenen Daten für die Zwecke der Verarbeitung nicht länger benötigt, Sie diese jedoch zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigen, oder</p>

                <p>d) wenn Sie Widerspruch gegen die Verarbeitung gemäß Artikel 21 Absatz 1 DSGVO eingelegt haben, solange noch nicht feststeht, ob die berechtigten Gründe des Verantwortlichen gegenüber Ihren überwiegen.</p>

                <p>Wurde die Verarbeitung gemäß den oben genannten Voraussetzungen eingeschränkt, so werden diese personenbezogenen Daten – von ihrer Speicherung ab-gesehen – nur mit Ihrer Einwilligung oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen oder zum Schutz der Rechte einer anderen natürlichen oder juristischen Person oder aus Gründen eines wichtigen öffentlichen Interesses der Union oder eines Mitgliedstaats verarbeitet.</p>

                <p>Um das Recht auf Einschränkung der Verarbeitung geltend zu machen, können Sie sich jederzeit an uns unter den oben angegebenen Kontaktdaten wenden.</p>

                <h4>(7) Recht auf Datenübertragbarkeit</h4>

                <p>Sie haben das Recht, die Sie betreffenden personenbezogenen Daten, die Sie uns bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren For-mat zu erhalten, und Sie haben das Recht, diese Daten einem anderen Verantwortlichen ohne Behinderung durch den Verantwortlichen, dem die personenbezogenen Daten bereitgestellt wurden, zu übermitteln, sofern:</p>

                <p>a) die Verarbeitung auf einer Einwilligung gemäß Artikel 6 Absatz 1 Buchstabe a oder Artikel 9 Absatz 2 Buchstabe a oder auf einem Vertrag gemäß Artikel 6 Absatz 1 Buchstabe b DSGVO beruht und</p>

                <p>b) die Verarbeitung mithilfe automatisierter Verfahren erfolgt.</p>

                <p>Bei der Ausübung des Rechts auf Datenübertragbarkeit haben Sie das Recht, zu erwirken, dass die personenbezogenen Daten direkt von einem Verantwortlichen zu einem anderen Verantwortlichen übermittelt werden, soweit dies technisch machbar ist. Die Ausübung des Rechts auf Datenübertragbarkeit lässt das Recht auf Löschung („Recht auf Vergessenwerden“) unberührt. Dieses Recht gilt nicht für eine Verarbeitung, die für die Wahrnehmung einer Aufgabe erforderlich ist, die im öffentlichen Interesse liegt oder in Ausübung öffentlicher Gewalt erfolgt, die dem Verantwortlichen übertragen wurde.</p>

                <h4>(8) Widerspruchsrecht</h4>

                <p>Sie haben das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung Sie betreffender personenbezogener Daten, die aufgrund von Artikel 6 Absatz 1 Buchstaben e oder f DSGVO erfolgt, Widerspruch einzulegen; dies gilt auch für ein auf diese Bestimmungen gestütztes Profiling. Der Verantwortliche verarbeitet die personenbezogenen Daten nicht mehr, es sei denn, er kann zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und Freiheiten der betroffenen Person überwiegen, oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.</p>

                <p>Werden personenbezogene Daten verarbeitet, um Direktwerbung zu betreiben, so haben Sie das Recht, jederzeit Widerspruch gegen die Verarbeitung Sie betreffender personenbezogener Daten zum Zwecke derartiger Werbung einzulegen; dies gilt auch für das Profiling, soweit es mit solcher Direktwerbung in Verbindung steht. Widersprechen Sie der Verarbeitung für Zwecke der Direktwerbung, so werden die personenbezogenen Daten nicht mehr für diese Zwecke verarbeitet.</p>

                <p>Im Zusammenhang mit der Nutzung von Diensten der Informationsgesellschaft kön-ne Sie ungeachtet der Richtlinie 2002/58/EG Ihr Widerspruchsrecht mittels automatisierter Verfahren ausüben, bei denen technische Spezifikationen verwendet werden.</p>

                <p>Sie haben das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, gegen die Sie betreffende Verarbeitung Sie betreffender personenbezogener Daten, die zu wissenschaftlichen oder historischen Forschungszwecken oder zu statistischen Zwecken gemäß Artikel 89 Absatz 1 erfolgt, Widerspruch einzulegen, es sei denn, die Verarbeitung ist zur Erfüllung einer im öffentlichen Interesse liegenden Aufgabe erforderlich.</p>

                <p>Das Widerspruchsrecht können Sie jederzeit ausüben, indem Sie sich an den jeweiligen Verantwortlichen wenden.</p>

                <h4>(9) Automatisierte Entscheidungen im Einzelfall einschließlich Profiling</h4>

                <p>Sie haben das Recht, nicht einer ausschließlich auf einer automatisierten Verarbei-tung – einschließlich Profiling – beruhenden Entscheidung unterworfen zu werden, die Ihnen gegenüber rechtliche Wirkung entfaltet oder Sie in ähnlicher Weise erheblich beeinträchtigt. Dies gilt nicht, wenn die Entscheidung:</p>

                <p>a) für den Abschluss oder die Erfüllung eines Vertrags zwischen der betroffenen Person und dem Verantwortlichen erforderlich ist,</p>

                <p>b) aufgrund von Rechtsvorschriften der Union oder der Mitgliedstaaten, denen der Verantwortliche unterliegt, zulässig ist und diese Rechtsvorschriften an-gemessene Maßnahmen zur Wahrung Ihrer Rechte und Freiheiten sowie der berechtigten Interessen der betroffenen Person enthalten oder</p>

                <p>c) mit Ihrer ausdrücklichen Einwilligung erfolgt.</p>

                <p>Der Verantwortliche trifft angemessene Maßnahmen, um Ihre Rechte und Freiheiten sowie die berechtigten Interessen zu wahren, wozu mindestens das Recht auf Erwirkung des Eingreifens einer Person seitens des Verantwortlichen, auf Darlegung des eigenen Standpunkts und auf Anfechtung der Entscheidung gehört.</p>

                <p>Dieses Recht können Sie jederzeit ausüben, indem sie sich an den jeweiligen Ver-antwortlichen wenden.</p>

                <h4>(10) Recht auf Beschwerde bei einer Aufsichtsbehörde</h4>

                Sie haben zudem, unbeschadet eines anderweitigen verwaltungsrechtlichen oder gerichtlichen Rechtsbehelfs, das Recht auf Beschwerde bei einer Aufsichtsbehörde, insbesondere in dem Mitgliedstaat Ihres Aufenthaltsorts, Ihres Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes, wenn Sie der Ansicht sind, dass die Verarbeitung der Sie betreffenden personenbezogenen Daten gegen diese Verordnung verstößt.

                <h4>(11) Recht auf wirksamen gerichtlichen Rechtsbehelf</h4>

                <p>Jede natürliche oder juristische Person hat unbeschadet eines anderweitigen verwaltungsrechtlichen oder außergerichtlichen Rechtsbehelfs das Recht auf einen wirksamen gerichtlichen Rechtsbehelf gegen einen sie betreffenden rechtsverbindlichen Beschluss einer Aufsichtsbehörde.</p>

                <h3>Erhebung von Daten beim Besuch unserer Website</h3>

                <p>Bei der bloß informatorischen Nutzung der Website, also wenn Sie sich nicht registrieren oder uns anderweitig Informationen übermitteln, erheben wir nur die Daten, die Ihr Browser an unseren Server übermittelt.</p>

                <p>Erhoben werden unter anderem folgende Daten:</p>

                <p>Ihre IP-Adresse<br>
                das Datum und die Uhrzeit der Anfrage<br>
                die Zeitzonendifferenz zur Greenwich Mean Time (GMT)<br>
                den Inhalt der Anforderung (konkrete Seite)<br>
                den Zugriffsstatus/HTTP-Statuscode<br>
                die jeweils übertragene Datenmenge<br>
                die Website, von der das zugreifende System auf unsere Internetseite gelangt („Referrer“)<br>
                Ihren Browsertyp, -version und -sprache<br>
                Ihr Betriebssystem und dessen Oberfläche<br>
                den Internetservicebetreiber des zugreifenden Systems<br>
                die Sprache und Version Ihrer Browsersoftware</p>

                <h3>Einsatz von Cookies</h3>

                <p>(1) Zusätzlich zu den zuvor genannten Daten werden bei der Nutzung unserer Website Cookies auf Ihrem Rechner gespeichert. Bei Cookies handelt es sich um kleine Textdateien, die auf Ihrer Festplatte dem von Ihnen verwendeten Browser zugeordnet gespeichert werden und durch welche der Stelle, die den Cookie setzt, bestimmte Informationen zufließen. Cookies können keine Programme ausführen oder Viren auf Ihren Computer übertragen. Sie dienen dazu, das Internetangebot insgesamt nutzerfreundlicher und effektiver zu machen.</p>

                <p>(2) Diese Website nutzt sog. transiente und persistente Cookies, deren Umfang und Funktionsweise im Folgenden erläutert werden:</p>

                <p>a) Transiente Cookies werden automatisiert gelöscht, wenn Sie den Browser schließen. Dazu zählen insbesondere die Session-Cookies. Diese speichern eine sogenannte Session-ID, mit welcher sich verschiedene Anfragen Ihres Browsers der gemeinsamen Sitzung zuordnen lassen. Dadurch kann Ihr Rechner wiedererkannt werden, wenn Sie auf unsere Website zurückkehren. Die Session-Cookies werden gelöscht, wenn Sie sich ausloggen oder den Browser schließen.</p>

                <p>b) Persistente Cookies werden automatisiert nach einer vorgegebenen Dauer gelöscht, die sich je nach Cookie unterscheiden kann. Sie können die Cookies in den Sicherheitseinstellungen Ihres Browsers jederzeit löschen.</p>

                <p>Sie können Ihre Browser-Einstellung entsprechend Ihren Wünschen konfigurieren und z. B. die Annahme von Third-Party-Cookies oder allen Cookies ablehnen. Sog. „Third Party Cookies“ sind Cookies, die durch einen Dritten gesetzt wurden, folglich nicht durch die eigentliche Website auf der man sich gerade befindet. Wir weisen Sie darauf hin, dass Sie durch die Deaktivierung von Cookies eventuell nicht alle Funktionen dieser Website nutzen können.</p>

                <h3>Matomo</h3>

                <p>Der für die Verarbeitung Verantwortliche hat auf dieser Internetseite die Komponente Matomo integriert. Matomo ist ein Open-Source-Softwaretool zur Web-Analyse. Web-Analyse ist die Erhebung, Sammlung und Auswertung von Daten über das Verhalten von Besuchern von Internetseiten. Ein Web-Analyse-Tool erfasst unter anderem Daten darüber, von welcher Internetseite eine betroffene Person auf eine Internetseite gekommen ist (sogenannter Referrer), auf welche Unterseiten der Internetseite zugegriffen oder wie oft und für welche Verweildauer eine Unterseite betrachtet wurde. Eine Web-Analyse wird überwiegend zur Optimierung einer Internetseite und zur Kosten-Nutzen-Analyse von Internetwerbung eingesetzt.</p>

                <p>Die Software wird auf dem Server des für die Verarbeitung Verantwortlichen betrieben, die datenschutzrechtlich sensiblen Logdateien werden ausschließlich auf diesem Server gespeichert.</p>

                <p>Der Zweck der Matomo-Komponente ist die Analyse der Besucherströme auf unserer Internetseite. Der für die Verarbeitung Verantwortliche nutzt die gewonnenen Daten und Informationen unter anderem dazu, die Nutzung dieser Internetseite auszuwerten, um Online-Reports, welche die Aktivitäten auf unseren Internetseiten aufzeigen, zusammenzustellen.</p>

                <p>Matomo setzt ein Cookie auf dem informationstechnologischen System der betroffenen Person. Was Cookies sind, wurde oben bereits erläutert. Mit der Setzung des Cookies wird uns eine Analyse der Benutzung unserer Internetseite ermöglicht. Durch jeden Aufruf einer der Einzelseiten dieser Internetseite wird der Internetbrowser auf dem informationstechnologischen System der betroffenen Person automatisch durch die Matomo-Komponente veranlasst, Daten zum Zwecke der Online-Analyse an unseren Server zu übermitteln. Im Rahmen dieses technischen Verfahrens erhalten wir Kenntnis über personenbezogene Daten, wie der IP-Adresse der betroffenen Person, die uns unter anderem dazu dient, die Herkunft der Besucher und Klicks nachzuvollziehen.</p>

                <p>Mittels des Cookies werden personenbezogene Informationen, beispielsweise die Zugriffszeit, der Ort, von welchem ein Zugriff ausging und die Häufigkeit der Besuche auf unserer Internetseite gespeichert. Bei jedem Besuch unserer Internetseiten werden diese personenbezogenen Daten, einschließlich der IP-Adresse des von der betroffenen Person genutzten Internetanschlusses, an unseren Server übertragen. Diese personenbezogenen Daten werden durch uns gespeichert. Wir geben diese personenbezogenen Daten nicht an Dritte weiter.</p>

                <p>Die betroffene Person kann die Setzung von Cookies durch unsere Internetseite, wie oben bereits dargestellt, jederzeit mittels einer entsprechenden Einstellung des genutzten Internetbrowsers verhindern und damit der Setzung von Cookies dauerhaft widersprechen. Eine solche Einstellung des genutzten Internetbrowsers würde auch verhindern, dass Matomo ein Cookie auf dem informationstechnologischen System der betroffenen Person setzt. Zudem kann ein von Matomo bereits gesetzter Cookie jederzeit über einen Internetbrowser oder andere Softwareprogramme gelöscht werden.</p>

                <p>Ferner besteht für die betroffene Person die Möglichkeit, einer Erfassung der durch den Matomo erzeugten, auf eine Nutzung dieser Internetseite bezogenen Daten zu widersprechen und eine solche zu verhindern. Hierzu muss die betroffene Person in Ihrem Browser "Do Not Track" einstellen.</p>

                <p>Mit der Setzung des Opt-Out-Cookies besteht jedoch die Möglichkeit, dass die Internetseiten des für die Verarbeitung Verantwortlichen für die betroffene Person nicht mehr vollumfänglich nutzbar sind. Weitere Informationen und die geltenden Datenschutzbestimmungen von Matomo können abgerufen werden unter 
                https://matomo.org/privacy/</p>

                <p>(Dieser Teil der Datenschutzerklärung wurde durch den Datenschutzerklärungs-Generator der DGD Deutsche Gesellschaft für Datenschutz GmbH, die als Externer Datenschutzbeauftragter Essen tätig ist, in Kooperation mit den Datenschutz Anwälten der Kanzlei WILDE BEUGER SOLMECKE | Rechtsanwälte erstellt.)</p>

                

                <h3>Auftragsverarbeiter</h3>

                <p>Wir bedienen uns externer Dienstleister (Auftragsverarbeiter) z. B. für den Versand von Newslettern. Mit dem Dienstleister wurde eine separate Auftragsdatenverarbei-tung geschlossen, um den Schutz Ihrer personenbezogenen Daten zu gewährleisten.</p>

                <p>Wir arbeiten mit folgenden Dienstleistern zusammen:</p>

                <p>Informationsverarbeitung Leverkusen GmbH<br>
                Overfeldweg 55<br>
                51371 Leverkusen<br>
                E-Mail: ivlivlde <br>
                https://www.ivl.de</p>

                <p>Sitepark Gesellschaft für Informationsmanagement mbH<br>
                Neubrückenstraße 8-11<br>
                48143 Münster<br>
                E-Mail: info2018sitepark.com<br>
                Internet: https://www.sitepark.com</p>

                <h3>Kontaktmöglichkeiten mit der Stadt</h3>

                <h4>Per Post</h4>
                <p>Bei postalischem Kontakt erfolgt über die zentrale Poststelle der Stadt Leverkusen eine Weiterleitung ihres Anliegens an den zuständigen Fachbereich/die zuständige Fachabteilung. Die von Ihnen übermittelten personenbezogenen Daten (z.B. Name, Vorname, Anschrift) werden zum Zwecke der Kontaktaufnahme und zur Bearbeitung Ihres Anliegens im erforderlichen Umfang verarbeitet. 
                (Rechtsgrundlage für die Verarbeitung der Daten ist Art. 6 Abs. 1 lit. e), Abs. 3 DSGVO i.V.m. § 3 Abs. 1 DSG NRW.)</p>

                <h4>Per Telefon</h4>
                <p>Der telefonische Kontakt mit der Stadt Leverkusen kann über die zentrale Rufnummer 406-0 (Bürgertelefon) oder über eine direkte Durchwahl zur zuständigen Stelle erfolgen.</p>

                <p>Die Stadt Leverkusen bedient sich bei der zentralen Rufnummer (Bürgertelefon) des Callcenters der Stadt Köln. Dort werden, soweit dies zur Bearbeitung ihres Anliegens erforderlich ist, Ihre personenbezogenen Daten verarbeitet. Soweit Ihr Anliegen dort nicht abschließend bearbeitet werden kann, erfolgt eine Weiterleitung an den zuständigen Fachbereich/die zuständige Fachabteilung innerhalb der Stadt Leverkusen. Dort werden Ihre Daten – wie im Falle einer direkten Durchwahl – zum Zwecke der Kontaktaufnahme und zur Bearbeitung Ihres Anliegens im erforderlichen Umfang verarbeitet.
                (Rechtsgrundlage für die Verarbeitung der Daten ist Art. 6 Abs. 1 lit. e), Abs. 3 DSG-VO i.V.m. § 3 Abs. 1 DSG NRW.)</p>

                <h4>Per E-Mail, über das Kontaktformular oder über postmasterstadt.leverkusen.de</h4>
                <p>Die Internetseite der Stadt Leverkusen enthält aufgrund von gesetzlichen Vorschriften Angaben, die eine schnelle elektronische Kontaktaufnahme sowie eine unmittelbare Kommunikation mit uns ermöglichen.
                - Funktionspostfächer, personenbezogene Postfächer oder über postmasterstadt.leverkusen.de</p>

                <p>Sofern Sie per E-Mail (Funktionspostfächer, personenbezogene Postfächer oder über postmasterstadt.leverkusende) mit uns in Kontakt treten, werden die von Ihnen übermittelten personenbezogenen Daten zum Zwecke der Kontaktaufnahme und zur Bearbeitung Ihres Anliegens im erforderlichen Umfang verarbeitet. Dabei erfolgt ggf. eine Weitergabe/Weiterleitung an den zuständigen Fachbereich/die zuständige Fachabteilung.
                (Rechtsgrundlage für die Verarbeitung der Daten ist Art. 6 Abs. 1 lit. e), Abs. 3 DSGVO i.V.m. § 3 Abs. 1 DSG NRW.) </p>

                <h4>- Kontaktformular</h4>
                <p>Soweit Sie das Kontaktformular zur Kommunikation verwenden, werden die von Ihnen übermittelten personenbezogenen Daten zum Zwecke der Kontaktaufnahme und zur Bearbeitung Ihres Anliegens im erforderlichen Umfang verarbeitet. Dabei erfolgt eine Weitergabe/Weiterleitung an den zuständigen Fachbereich/die zuständige Fachabteilung. 
                Die Angabe Ihres Namens und Vornamens sowie Ihrer E-Mailadresse ist erforderlich. Die Angabe der Telefonnummer ist optional und ermöglicht uns, soweit gewünscht, eine telefonische Kontaktaufnahme.
                Die Verarbeitung der übermittelten Daten erfolgt auf der Grundlage Ihrer Einwilligung gem. Art. 6 Abs. 1 lit. a) DSGVO. Diese kann jederzeit ohne Angabe von Gründen mit Wirkung für die Zukunft widerrufen werden.</p>

                <p>Die anfallenden personenbezogenen Daten, die wir erhalten, speichern wir, solange dies für den von Ihnen verfolgten Zweck oder im Zusammenhang mit damit ausgelösten Verwaltungsvorgängen und den hierfür geltenden Aufbewahrungspflichten erforderlich ist oder sofern dies durch Gesetze oder andere Rechtsvorschriften, welchen die Stadt Leverkusen unterliegt, vorgesehen ist.
                Bitte beachten Sie, dass eine verschlüsselte Übertragung Ihrer personenbezogenen Daten nur bei Nutzung des Kontaktformulars jedoch nicht bei Nutzung der Funktionspostfächer oder personenbezogenen Postfächer erfolgt.</p>

                <p>Wir verweisen auf die weiteren Ausführungen zur <a href="https://www.leverkusen.de/service/elektronische-kommunikation-neu.php">elektronischen Kommunikation</a>.</p>

                <h3>Nutzung Social Media</h3>

                <p>Die Stadt Leverkusen unterhält aktuell unter Leverkusen.de eine Onlinepräsenz innerhalb des sozialen Netzwerks Facebook, um mit den dort aktiven Kunden, Interessenten und Nutzern kommunizieren und sie dort über unsere Leistungen informieren zu können. 
                Wir weisen darauf hin, dass dabei Daten der Nutzer außerhalb des Raumes der Europäischen Union verarbeitet werden können. Hierdurch können sich für die Nutzer Risiken ergeben, weil so zum Beispiel die Durchsetzung der Rechte der Nutzer erschwert werden könnte. 
                Im Hinblick auf US-Anbieter die unter dem Privacy-Shield zertifiziert sind, weisen wir darauf hin, dass sie sich damit verpflichten, die Datenschutzstandards der EU einzuhalten.</p>

                <p>Ferner werden die Daten der Nutzer im Regelfall für Marktforschungs- und Werbezwecke verarbeitet. So können z.B. aus dem Nutzungsverhalten und sich daraus ergebenden Interessen der Nutzer Nutzungsprofile erstellt werden. Die Nutzungsprofile können wiederum verwendet werden, um z.B. Werbeanzeigen innerhalb und außerhalb der Plattformen zu schalten, die mutmaßlich den Interessen der Nutzer entsprechen. Zu diesen Zwecken werden im Regelfall Cookies auf den Rechnern der Nutzer gespeichert, in denen das Nutzungsverhalten und die Interessen der Nutzer gespeichert werden.</p> 

                <p>Ferner können in den Nutzungsprofilen auch Daten unabhängig der von den Nutzern verwendeten Geräte gespeichert werden (insbesondere wenn die Nutzer Mitglieder der jeweiligen Plattformen sind und bei diesen eingeloggt sind).</p> 

                <p>Die Verarbeitung der personenbezogenen Daten der Nutzer erfolgt auf Grundlage unserer berechtigten Interessen an einer effektiven Information der Nutzer und Kommunikation mit den Nutzern gem. Art. 6 Abs. 1 lit. f. DSGVO.</p> 

                <p>Falls die Nutzer von den jeweiligen Anbietern um eine Einwilligung in die Datenverarbeitung gebeten werden (d.h. ihr Einverständnis z.B. über das Anhaken eines Kontrollkästchens oder Bestätigung einer Schaltfläche erklären) ist die Rechtsgrundlage der Verarbeitung Art. 6 Abs. 1 lit. a., Art. 7 DSGVO.</p>

                <p>Für eine detaillierte Darstellung der jeweiligen Verarbeitungen und der Widerspruchsmöglichkeiten (Opt-Out), verweisen wir auf die nachfolgend verlinkten Angaben der Anbieter. Auch im Fall von Auskunftsanfragen und der Geltendmachung von Nutzerrechten, weisen wir darauf hin, dass diese am effektivsten bei den Anbietern geltend gemacht werden können. Nur die Anbieter haben jeweils Zugriff auf die Daten der Nutzer und können direkt entsprechende Maßnahmen ergreifen und Auskünfte geben. <br>
                Sollten Sie dennoch Hilfe benötigen, dann können Sie sich wenden an:<br>
                Facebook (Facebook Ireland Ltd., 4 Grand Canal Square, Grand Canal Harbour, Dublin 2, Irland) </p>

                - Datenschutzerklärung: <a href="https://www.facebook.com/about/privacy/" target="_blank">https://www.facebook.com/about/privacy/</a><br>
                - Opt-Out: <a href="https://www.facebook.com/login.php?next=https%3A%2F%2Fwww.facebook.com%2Fadpreferences%2Fad_settings%2F%3Fentry_product%3Daccount_settings_menu" target="_blank">https://www.facebook.com/settings?tab=ads</a>
                und<br>
                - <a href="https://www.youronlinechoices.com" target="_blank">http://www.youronlinechoices.com</a>
                <br>Privacy Shield:<br>
                <a href="https://www.privacyshield.gov/participant?id=a2zt0000000GnywAAC&status=Active" target="_blank">https://www.privacyshield.gov/participant?id=a2zt0000000GnywAAC&status=Active</a>

                <p>(Erstellt mit Datenschutz-Generator.de von RA Dr Thomas Schwenke)</p>


                <h4>"Teilen" durch Links</h4>

                <p>Die Internetseite der Stadt Leverkusen verweist ausschließlich durch Links auf externe soziale Netzwerke, wie Facebook, Twitter oder Google+. Erkennbar ist das "Teilen" durch das jeweilige Logo.Es werden keine sogenannten Plugins genutzt.</p>

                <p>Erst durch die aktive Nutzung der Links werden die externen Seiten der sozialen Netzwerke aufgerufen. Den Datenschutzhinweisen von Facebook, Twitter und Google+ können Sie dann jeweils entnehmen, in welchem Umfang und zu welchem Zweck dort Ihre Daten erhoben werden und inwiefern Sie diese anpassen können.</p>
                

                <a href="/" class="btn btn-secondary text-white">zur Startseite</a>
                
            </div>
        </div>
    </div>


     
</x-guest-layout>
