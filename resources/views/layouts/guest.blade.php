<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Erziehersuche Leverkusen') }}</title>


        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <!-- Add the slick-theme.css if you want default styling -->
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
        <!-- Add the slick-theme.css if you want default styling -->
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>


        
        <script src="https://kit.fontawesome.com/802e5b60ae.js" crossorigin="anonymous"></script>
       
    </head>
    <body class="bg-light font-sans antialiased">
         <nav class="navbar navbar-expand-lg navbar-light shadow-sm fixed-top" style="background-color: #fff;">
          <a class="navbar-brand" href="/">
            <img class="img-fluid logo" src="/images/Logo.png" align="Logo Stadt Leverkusen">
         </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav ml-auto">

              <li class="nav-item">
                <a class="nav-link text-white btn btn-primary mr-2 my-1" href="mailto:carmen.meiners@sportpark-lev.de">E-Mail</a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-white btn btn-primary mr-2 my-1" href="tel:02144062420">Tel.: 0214 - 406 24 20</a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-white btn btn-secondary my-1" href="https://recruitingapp-5165.de.umantis.com/Jobs/1?lang=ger" target="_blank">Online bewerben</a>
              </li>
            </ul>
          </div>
        </nav>
        <main>
            {{ $slot }}
        </main>

        <footer class="bg-light">
            <div class="container-fluid">
                <div class="row py-4">
                    <div class="col-12 ml-auto mr-auto">
                        <div class="row">
                            <div class="col-lg-6 align-self-center">
                                <img class="img-fluid logoFooter mr-3" src="/images/Logo.png">
                                <a style="font-size: 1.6em; font-weight: 500;" href="https://www.leverkusen.de" target="_blank" class="text-dark">Stadt Leverkusen</a>
                            </div>
                              <div class="col-lg-6 ml-auto align-self-center text-right">
                                <span class="text-dark mr-2">© Stadt Leverkusen 2021</span>
                                <a href="/impressum" class="text-dark mr-2">Impressum</a>
                                <a href="/datenschutz" class="text-dark">Datenschutzerklärung</a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </footer>
        

        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}"></script>
        <!-- GSAP -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.0.0/gsap.min.js" integrity="sha512-gNmE7RbgFn8AIEtbCw00Ub5JYgsSRyQ1rvim/sVuzJcDCrhUwAjWCOlEgi9yr7xJBAgesPoUq6xzhci0pn7+qw==" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js" integrity="sha512-HzAEuXwhLxwm/Jj+5ecdjzrRVrjuh2ZeIxyT1Ln37TO5pWNMnKBuU7cfd1wvRQ/k86w1oC174Yk1T7aRlBpIcA==" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
    </body>
</html>

<script>
  
  $('#slider').slick({
    dots: true,
    centerMode: true,
    centerPadding: '15vw',
    slidesToShow: 1,
    prevArrow:'#prevSlide',
    nextArrow:'#nextSlide',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '15vw',
          slidesToShow: 1
        }
      }
    ]
  });

  var tween = new TimelineMax()
      .from(".animate", 1, {opacity: 0},0);

       

  var controller = new ScrollMagic.Controller();
        
  new ScrollMagic.Scene({
      triggerElement: "#startAnimation"
  })
  .setTween(tween)
  .addTo(controller);    
       
</script>