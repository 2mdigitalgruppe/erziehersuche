<x-guest-layout>

    <div class = " d-none d-md-block" >
        <header id = "header" style = "position:relative;">
            <div id = "slider">
                <img src = "/images/Stadt_Leverkusen_Webbilder.jpg" class = "img-fluid" alt="Weg zum Ferienhaus" title="">
                <img src = "/images/Stadt_Leverkusen_Webbilder2.jpg" class = "img-fluid" alt="Nordsee" title="">
                <img src = "/images/Stadt_Leverkusen_Webbilder3.jpg" class = "img-fluid" alt="Blick aus dem Fenster" title="">
            </div>
            <div id = "prevSlide" class = "arrow">
                <div class = "position-relative d-none d-lg-block" style = "height: 100%;">
                    <div class = "row" style = "height: 100%;">
                        <div class = "col-12 align-self-center text-right">
                            <span class="fa-stack fa-2x" style = "margin-right:-30px;">
                              <i class="fas fa-square fa-stack-2x" style = "color:rgb(85,85,85);"></i>
                              <i class="fal fa-chevron-left fa-stack-1x fa-inverse"></i>
                            </span>
                        </div>                           
                    </div>
                </div>
            </div>
            <div id = "nextSlide" class = "arrow">
                <div class = "position-relative d-none d-lg-block" style = "height: 100%;">
                    <div class = "row" style = "height: 100%;">
                        <div class = "col-12 align-self-center text-left">
                            <span class="fa-stack fa-2x" style = "margin-left:-30px;">
                              <i class="fas fa-square fa-stack-2x" style = "color:rgb(85,85,85);"></i>
                              <i class="fal fa-chevron-right fa-stack-1x fa-inverse"></i>
                            </span>
                        </div>                           
                    </div>
                </div>
            </div>
        </header>
    </div>

    <div class = " d-block d-md-none" >
        <img class="img-fluid" src="/images/Stadt_Leverkusen_Webbilder.jpg">
    </div>

    <div class = "container-fluid">
        <div class = "row py-5 welcome">
            <div class = "col-lg-10 ml-auto mr-auto">
                
                <div class="pt-5">
                    <span class="text-uppercase py-2 px-3 text-white bg-primary headline">Wir bieten</span>
                </div>
              

                <div id = "startAnimation" class="row py-5">
                    <div class="col-lg-4">
                        <ul class = "animate">
                            <li><i class="far fa-check mr-2 text-primary"></i>sicherer Arbeitsplatz in Leverkusen</li>
                            <li><i class="far fa-check mr-2 text-primary"></i>30 Tage Urlaub</li>
                            <li><i class="far fa-check mr-2 text-primary"></i>Bezahlung nach TVöD-Tarifvertrag sowie - bei Beschäftigten - jährliche Sonderzahlung</li>
                            <li><i class="far fa-check mr-2 text-primary"></i>deutlich vergünstigtes Jobticket</li>
                        </ul>
                    </div>
                    <div class="col-lg-4 ">
                         <ul class = "animate">

                            
                            <li><i class="far fa-check mr-2 text-primary"></i>vielfältige Einsatzmöglichkeiten in einer von insgesamt 41 Städtischen Kitas</li>
                            
                            
                            <li><i class="far fa-check mr-2 text-primary"></i>Fort- und Weiterbildungsmöglichkeiten</li>
                            <li><i class="far fa-check mr-2 text-primary"></i>Einarbeitung durch ein nettes sowie fachlich versiertes Team</li>
                        </ul>
                    </div>
                     <div class="col-lg-4">
                         <ul class = "animate">
                            
                            <li><i class="far fa-check mr-2 text-primary"></i>Betriebliches Gesundheitsmanagement</li>
                            <li><i class="far fa-check mr-2 text-primary"></i>Betriebsrente der Rheinischen Versorgungskasse für tariflich Beschäftigte</li>
                            <li><i class="far fa-check mr-2 text-primary"></i>corporate benefits – Mitarbeiterangebote mit Sonderkonditionen namhafter Hersteller und Marken</li>
                        </ul>
                    </div>
                </div>

                
            </div>
        </div>
        <div class = "row py-5">
            <div class = "col-lg-10 ml-auto mr-auto">
                
                <span class="text-uppercase py-2 px-3 text-white bg-primary headline">Wir suchen Sie</span>
                <div class="pb-5 pt-1">
                    <span class="text-uppercase py-2 px-3 text-white bg-secondary subtitle">Erzieher & Kinderpfleger (m/w/d)</span>
                </div>
            </div>
        </div>

        <div class = "row pb-5 pt-3">
            <div class = "col-lg-10 ml-auto mr-auto">
              

                <div class="row py-5">
                    <div class="col-lg-6 py-5">
                        <div class="box shadow-sm rounded px-4 py-3 bg-white">
                            <div class="text-center row py-3" style="margin-top: -120px;">
                                <div class="col-lg-4 col-9 ml-auto mr-auto">
                                    <img class="img-fluid" src="/images/erzieher_team.png">
                                </div>
                            </div>
                            <h3 class="pt-3">Erzieherin/Erzieher (m/w/d)</h3>
                            <div class="py-2">
                                <span class="badge badge-secondary text-white" style="background-color: #49aae2;">Wir suchen Sie!</span>
                            </div>

                            <p>Der Fachbereich Kinder und Jugend der Stadt Leverkusen sucht ab sofort Erzieherinnen bzw. Erzieher (m/w/d) (S 8a TVöD SuE) für verschiedene Kindertageseinrichtungen in Vollzeit/ Teilzeit für unbefristete wie befristete Stellen.</p>

                            <div class="py-2">
                                <span class="badge badge-primary text-white">Voraussetzung für eine Bewerbung</span>
                            </div>

                           
                            <ul class="pt-1">
                                <li>Staatlich anerkannte Erzieherinnen und Erzieher</li>
                                <li>Staatlich anerkannte Heilpädagoginnen und Heilpädagogen</li>
                                <li>Staatlich anerkannte Heilerziehungspflegerinnen und Heilerziehungspfleger, die an einer Fachschule oder in entsprechenden doppeltqualifizierenden Bildungsgängen der Berufskollegs ausgebildet sind</li>
                                <li>Kinderkrankenschwestern und Kinderkrankenpfleger, die aufgrund ihrer besonderen Qualifikation vor allem für die Betreuung von Kindern mit besonderem pflegerischem Betreuungsbedarf eingesetzt werden</li>
                                <li>Absolventinnen und Absolventen von Studiengängen der sozialen Arbeit mit staatlicher Anerkennung</li>
                                <li>Absolventinnen und Absolventen von Diplom-, Bachelor- und Master-Studiengängen der Erziehungswissenschaften, der Heilpädagogik sowie Studiengängen der Fachrichtung Soziale Arbeit, Kindheitspädagogik sowie Sozialpädagogik, wenn sie einen Nachweis über eine insgesamt mindestens sechsmonatige Praxiserfahrung in der Kindertagesbetreuung erbringen</li>
                            </ul>

                               <div class="py-2">
                                <span class="badge badge-secondary text-white">Aufgabengebiet</span>
                            </div>

                                <ul class="pt-1">
                                    <li>Förderung und Entwicklung von Kindern, auch von Kindern mit Förderungsbedarfen</li>
                                    <li>Durchführung und Anleitung kreativer Angebote zur Förderung</li>
                                    <li>Planung, Gestaltung und Nachweisführung des Förder- und Entwicklungsprozesses</li>
                                    <li>Erstellung von Beobachtungs- und Entwicklungsprotokollen</li>
                                </ul>

                            
                            <div class="py-2">
                                <span class="badge badge-secondary text-white" style="background-color: #00a744;">Ansprechpartnerin</span>
                            </div>
                            <p class="pt-1">Ansprechpartnerinnen für den Fachbereich Kinder und Jugend in Bewerbungsangelegenheiten:</p>

                            <p>Dezernat IV/Personalwirtschaft<br>
                            Frau Meiners, Tel.: 02 14/4 06 - 24 20<br>
                            E-Mail: carmen.meiners@sportpark-lev.de</p>

                            <p>Frau Angermund, Tel.: 02 14/4 06 - 88 46<br>
                            E-Mail: agnes.angermund@stadt.leverkusen.de</p>


                            
                            
                            <a class="btn btn-primary text-white w-100" href="https://recruitingapp-5165.de.umantis.com/Vacancies/249/Application/New/1" target="_blank">Jetzt bewerben</a>
                        </div>
                    </div>
                    <div class="col-lg-6 py-5">
                        <div class="box shadow-sm rounded px-4 py-3 bg-white">
                            <div class="text-center row py-3" style="margin-top: -120px;">
                                <div class="col-lg-4 col-9 ml-auto mr-auto">
                                    <img class="img-fluid" src="/images/kita.png">
                                </div>
                            </div>
                            <h3 class="pt-3">Ergänzungskraft KiTa (Vollzeit/Teilzeit) (m/w/d)</h3>
                           

                            <div class="py-2">
                                <span class="badge badge-secondary text-white" style="background-color: #49aae2;">Wir suchen Sie!</span>
                            </div>

                            <p>Der Fachbereich Kinder und Jugend der Stadt Leverkusen sucht ab sofort Ergänzungskräfte (m/w/d) für verschiedene Kindertageseinrichtungen in Vollzeit/Teilzeit für unbefristete wie befristete Stellen.</p>

                            <div class="py-2">
                                <span class="badge badge-primary text-white">Voraussetzung für eine Bewerbung</span>
                            </div>

                           
                            <p class="pt-1">
                                Abschluss als Kinderpflegerin/-pfleger oder eine mindestens gleichwertige Ausbildung oder bei internen Bewerberinnen bzw. Bewerbern ersatzweise Berufserfahrung in einer Kindertagesstätte gem. § 2 der Vereinbarung zu den Grundsätzen über die Qualifikation und den Personalschlüssel nach § 26 Abs. 2 Nr. 3 KiBiz.
                            </p>

                           
                            
                            <div class="py-2">
                                <span class="badge badge-secondary text-white" style="background-color: #00a744;">Ansprechpartnerin</span>
                            </div>
                             

                            <p class="pt-1">Ansprechpartnerinnen für den Fachbereich Kinder und Jugend in Bewerbungsangelegenheiten:</p>

                            <p>Dezernat IV/Personalwirtschaft<br>
                            Frau Meiners, Tel.: 02 14/4 06 - 24 20<br>
                            E-Mail: carmen.meiners@sportpark-lev.de</p>

                            <p>Frau Angermund, Tel.: 02 14/4 06 - 88 46<br>
                            E-Mail: agnes.angermund@stadt.leverkusen.de</p>



                            
                            
                            <a class="btn btn-primary text-white w-100" href="https://recruitingapp-5165.de.umantis.com/Vacancies/250/Application/New/1" target="_blank">Jetzt bewerben</a>
                        </div>
                        <div class="row" style="padding-top: 22px;">
                            <div class="col-12">
                                <img class="img-fluid rounded" src="/images/lev-sucht.jpg">
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </div>
    </div>

     <div class="bg-secondary py-5">
        <div class="container-fluid">
            <div class="row pt-5">
                <div class="col-12 text-center">
                    <hr style="height:5px; width: 50px; background-color: #f5da2d;">
                    <p class="pt-2 text-white"><b>Fragen oder Interesse?</b></p>
                   <h2 class="pt-2 text-white">Nehmen Sie Kontakt zu uns auf</h2>
                </div>
            </div>
             <div class="row pt-3 pb-5">
                <div class="col-lg-4 ml-auto mr-auto text-center">
                    <p class="text-white pb-3">Sie haben Fragen zu den Ausschreibungen oder möchten einfach mit uns in Kontakt treten? Gern schreiben Sie uns eine E-Mail oder rufen uns an.</p>
                    <a class="btn btn-primary mx-1" href="tel:02144062420">Anrufen</a>
                    <a class="btn btn-primary mx-1 py-2 px-3" href="mailto:carmen.meiners@sportpark-lev.de">E-Mail schreiben</a>
                </div>
            </div>
        </div>
    </div>

<!--     <div class="d-none d-md-block">
        <div id="break" >
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-10 ml-auto mr-auto">
                         <div class="row py-5 align-self-center" style="height: 65vh;">
                            <div class="col-lg-3 ml-auto text-center align-self-center">
                                <img class="img-fluid" src="/images/erzieherin.png">
                            </div>
                             <div class="col-lg-7 ml-auto mr-auto align-self-center">
                                <img src="/images/quote.jpg" class="img-fluid" style="width: 150px;">
                                <div class="ml-5" style="margin-top: -50px;">
                                    <h3 class="">Jeder Tag ist anders und bietet neue Herausforderungen. Es macht mich glücklich mit Kindern zu arbeiten, aber auch die Unterstützung durch meine Kolleg(inn)en ist großartig.</h3>

                                    <p class="pt-3">Peter Müller,<br>
                                    Erzieher im Zwergenkindergarten</p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <div class="d-block d-md-none">
        <div class="container-fluid">
            <div class="row pt-5">
                <div class="col-10 ml-auto mr-auto">
                    <img class="img-fluid" src="/images/erzieherin.png">
                </div>
                <div class="row py-5 align-self-center">
                    <div class="col-10 ml-auto mr-auto align-self-center">
                        <img src="/images/quote.jpg" class="img-fluid" style="width: 150px;">
                        <div class="ml-4" style="margin-top: -50px;">
                            <h3 class="">Jeder Tag ist anders und bietet neue Herausforderungen. Es macht mich glücklich mit Kindern zu arbeiten, aber auch die Unterstützung durch meine Kolleg(inn)en ist großartig.</h3>

                            <p class="pt-3">Peter Müller,<br>
                            Erzieher im Zwergenkindergarten</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->


     
</x-guest-layout>
